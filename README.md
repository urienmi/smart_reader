
# Smart Reader

  
### Objectives

*Read as intelligently as possible a .txt file in php.*

### The program can found

 - Dates (in 4 differents formats)
 - Emails
 - Postal Codes
 - Phone Numbers
 - Prices
 - Percentages
 - SIRET Numbers
 - Websites URLs
 - TVA Numbers
 - *Coming soon - Invoice Numbers*