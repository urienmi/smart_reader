<?php
    require_once('Parser.php');
    require_once('Performance.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<style>
    * {
        box-sizing: border-box;
    }
    table {
        border-collapse: collapse;
    }
    table, th, td {
        border: 1px solid black;
    }
    th, td {
        
        width: 80px;
    }
</style>
    <pre>
        <?php
            $performance = new Performance();
            $performance->start();
            /**************************************************************/

            $path = './txt/_invoice3_table.txt';

            $parser = new Parser($path);
            //print_r($parser->getFileContent());
            $table = $parser->process();
            //$parser->test();

            /**************************************************************/
            $performance->stop();
            $performance->getResult();
        ?>
    <pre>
    <table>
        <thead>
            <tr>
                <?php foreach($parser->getHeader() as $value): ?>
                    <th><?= $value ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($table as $row): ?>
                <tr>
                    <?php foreach($row as $value): ?>
                        <?php if(is_string($value)): ?>
                            <td><?= trim($value) ?></td>
                        <?php else: ?>
                            <td>
                                <?php foreach($value as $val): ?>
                                <div><?= trim($val) ?></div>
                                <?php endforeach; ?>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>


