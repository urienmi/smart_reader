<?php

require_once('utils.php');

class Parser {

    private string $file_content;
    private $header;
    private $headerData = [
        'line' => null,
        'columns' => [],
    ];

    private array $data = [
        'header' => [
            'object' => [
                'terms' => ['objet', 'service', 'designation', 'désignation', 'dÉsignation', 'produit', 'article', 'description', 'presta', 'prestation'],
                'types' => ['string'],
            ],
            'quantity' => [
                'terms' => ['quantité', 'quantitÉ', 'quantite', 'qte', 'qty', 'qté', 'qtÉ', 'nombre', 'nb'],
                'types' => ['int'],
            ],
            'price_ht' => [
                'terms' => ['prix', 'prix unitaire', 'prix unit', 'prix ht', 'prix unitaire ht', 'hors taxe', 'prix hors taxe'],
                'types' => ['float', 'string'],
            ],
            'price_ttc' => [
                'terms' => ['prix ttc', 'prix unitaire ttc', 'prix unit ttc'],
                'types' => ['float', 'string'],
            ],
            'tax' => [
                'terms' => ['tva', 'tva %', '% tva', 'taxe', 'taux', 'taux tva'],
                'types' => ['float', 'string'],
            ],
            'tax_amount' => [
                'terms' => ['montant tva', 'total tva', 'tva total'],
                'types' => ['int', 'float', 'string'],
            ],
            'amount' => [
                'terms' => ['total', 'montant', 'montant ht', 'montant total', 'prix total', 'prix total ht'],
                'types' => ['float', 'string'],
            ],
        ]
    ];
    private array $properties = [];

    /**
     * Parser contructor
     * @param string $file_path
     *      Read the content of the file and set it in a variable
     */
    public function __construct(string $file_path) {
        $this->setFileContent($file_path);
    }

    /**
     * Main function
     */
    public function process() {
        $res = [];
        $ranges;
        $this->initProperties();
        $splitted_text = $this->splitText($this->getFileContent());
        foreach($splitted_text as $line_number => $line) {
            if($this->header) {
                $ranges = $this->setTableRange($this->header);
                array_splice($splitted_text, 0, $line_number + 1);
                break;
            } else {
                $this->header = $this->extractHeader($line, $this->data['header']);
            }
        }
        $typed_properties = array_filter($this->headerData['columns'], function($value) {
            return $value['types'];
        });
        
        foreach($splitted_text as $line_number => $line) {
            foreach($ranges as $key => $range) {
                if(!empty($line)) {
                    $column = $this->getColumn($line, $range['start'], $range['end']);
                    $res[$line_number][$key] = $column;
                }
            }
        }
        return $this->filterTable($res);
    }

    /**
     * Fill properties array with default values
     */
    private function initProperties() {
        foreach($this->data['header'] as $category_name => $category_value) {
            $this->properties[$category_name] = [
                'content' => [],
                'start' => null,
                'end' => null,
                'order' => 0,
                'exist' => false,
            ];
        }
    }

    /**
     * Filter table
     * @param array $table
     *      Array of rows and columns extracted from text
     * @return array
     *      Filtered array
     */
    public function filterTable(array $table): array {
        $filtered_table = $table;
        $final_table = [];
        $last_full = null;
        $valid_column = [];
        $last_value = null;
        $typed_properties = array_filter($this->headerData['columns'], function($value) {
            return $value['types'];
        });
        foreach($filtered_table as $row_number => $row) {
            foreach($row as $property => $value) {
                if($this->checkColumnType($value, $this->headerData['columns'][$property]['types'])) {
                    $valid_column[$row_number][$property] = 1;
                }
            }
            if(count($valid_column[$row_number]) === count($typed_properties)) {
                if($last_full) {
                    $final_table[] = $last_full;
                }
                $last_full = $row;
            } else {
                foreach($row as $property => $value) {
                    if(!empty(trim($value)) && $this->checkColumnType($value, $this->headerData['columns'][$property]['types'])) {
                        if(is_string($last_full[$property])) {
                            $last_value = $last_full[$property];
                            $last_full[$property] = [];
                            $last_full[$property][] = $last_value;
                            $last_full[$property][] = $value;
                        } else {
                            $last_full[$property][] = $value;
                        }
                    }
                }
            }
        }
        $final_table[] = $last_full;
        return $final_table;
    }

    /**
     * Search for data table header line like (Product  Quantity    Price   Total)
     * @param string $line
     *      Text line
     * @param array $termsCategory
     *      $this->data['header'] category (object, quantity, price_ht...)
     * @return string|null
     *      - string Header found
     *      - null No header found
     */
    public function extractHeader(string $line, array $termsCategory): ?string {
        $properties = [];
        $matched_properties = [];
        $splitted_line = preg_split('/ {2,}/', $line);
        foreach($splitted_line as $key => $row) {
            $properties[$key] = [
                'content' => $row,
                'tag' => null,
            ];
        }

        foreach($termsCategory as $terms_key => $terms) {
            foreach($properties as $key => $property) {
                if($this->propertyMatch($property['content'], $terms['terms'])) {
                    $properties[$key]['tag'] = $terms_key;
                }
            }
        }
        $valid_properties = array_filter($properties, function($value) {
            return !!$value['tag'];
        });
        if(count($valid_properties) >= 3) {
            $this->headerData['line'] = $line;
            foreach($properties as $key => $property) {
                $this->headerData['columns'][$property['content']]['content'] = $property['content'];
                $this->headerData['columns'][$property['content']]['tag'] = $property['tag'] ? $property['tag'] : null;
                $this->headerData['columns'][$property['content']]['types'] = $property['tag'] ? $this->data['header'][$property['tag']]['types'] : null;
            }
            return $line;
        }
        return null;
    }

    /**
     * Check if the given property is matching one of the terms in array
     * @param string $property
     * @param array $terms
     * @return bool
     */
    public function propertyMatch(string $property, array $terms): bool {
        usort($terms,'sortByLongestString');
        foreach($terms as $term) {
            if(mb_strtolower($property, 'UTF-8') == $term) {
                return true;
            }
        }
        return false;
    }

    /**
     * Text dividing based on the header properties positions
     * @param string $line
     *      Header line
     * @return array
     *      Array of property positions
     */
    public function setTableRange(string $line): array {
        $splitted_line = preg_split('/ {2,}/', $line);
        for($i = 0; $i < count($splitted_line); $i++) {
            if($i !== (count($splitted_line) - 1)) {
                $array[$splitted_line[$i]] = [
                    // start from position 0 if first column
                    'start' => $i === 0 ? 0 : mb_stripos($line, mb_strtolower($splitted_line[$i], 'UTF-8')),
                    'end' => (mb_stripos($line, mb_strtolower($splitted_line[$i + 1], 'UTF-8')) - 1),
                ];
            } else {
                $array[$splitted_line[$i]] = [
                    'start' => mb_stripos($line, mb_strtolower($splitted_line[$i], 'UTF-8')),
                    'end' => -1,
                ];
            }
        }
        return $array;
    }

    /**
     * Retrieve text column by line based on starting and ending point
     * @param string $line
     * @param int $start
     * @param int $end
     * @return string
     */
    public function getColumn(string $line, int $start, int $end): string {
        if($end === -1) {
            return substr($line, $start);
        } else {
            return substr($line, $start, ($end - $start));
        }
    }

    /**
     * Validate the column type
     * @param string|int|float $value
     * @param array|null types
     * @return bool
     */
    public function checkColumnType($value, $types): bool {
        if($types) {
            foreach($types as $type) {
                switch($type) {
                    case 'string':
                        return is_string($value);
                        break;
                    case 'float':
                        return !!floatval($value);
                        break;
                    case 'int':
                        return !!intval($value);
                        break;
                    default:
                        return false;
                        break;
                }
            }
        }
        return false;
    }

    /**
     * Split text by line
     * @param string $file_content
     * @return array
     */
    public function splitText(string $file_content): array {
        return explode("\n", $file_content);
    }

    public function test() {
        echo mb_internal_encoding();
        $string1 = trim('Objet                     Unité        Poids       Quantité  Prix unitaire  Montant        ');
        $string2 = trim('Produit 1                                          20        15,15 $US      303,00 $US');
        echo $string1."\n";
        echo $string2."\n";
        $array1 = str_split_unicode($string1, 1);
        $array2 = str_split_unicode($string2, 1);
        echo "\n";
        foreach($array1 as $value) {
            echo "$value|";
        }
        echo "\n";
        foreach($array2 as $value) {
            echo "$value|";
        }
        echo "\n";
        $ranges = $this->setTableRange($string1);
        print_r($ranges);
        echo substr($string1, $ranges['Prix unitaire']['start'], ($ranges['Prix unitaire']['end'] - $ranges['Prix unitaire']['start']))."\n";
        echo substr($string2, $ranges['Prix unitaire']['start'], ($ranges['Prix unitaire']['end'] - $ranges['Prix unitaire']['start']))."\n";
    }

    /**
     * FileContent getter
     * @return string
     */
    public function getFileContent(): string {
        return $this->file_content;
    }
    /**
     * FileContent setter
     * @param string $file_path
     * @return void
     */
    public function setFileContent(string $file_path): void {
        $this->file_content = file_get_contents($file_path);
    }

    public function getHeader(): array {
        return preg_split('/ {2,}/', $this->header);
    }

}
