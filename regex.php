<?php

    define('REGEX_DATE', '/^(0[1-9]|[12]\d|3[01])[\/,\-,\.](0[1-9]|1[0-2])[\/,\-,\.]([12]\d{3})$/');         // Format dd-mm-yyyy (work also with . and ,)
    define('REGEX_DATE_MONTH', '/^((0[1-9])|(1[0-2]))\/(\d{4})$/');                                          // Format mm/yyyy
    define('REGEX_DATE_INVERT', '/^([12]\d{3})[\/,\-,\.](0[1-9]|1[0-2])[\/,\-,\.](0[1-9]|[12]\d|3[01])$/');  // Format yyyy-mm-dd (work also with . and ,) 
    define('REGEX_DATE_LONG', '/^((31(?!\ (F(é|e)?v(rier)?|Avr(il)?|Juin?|(Sep(?=\b|t)t?|Nov)(embre)?)))|((30|29)(?!\ F(é|e)?v(rier)?))|(29(?=\ F(é|e)?v(rier)?\ (((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)))))|(0?[1-9])|1\d|2[0-8])\ (Jan(vier)?|F(é|e)?v(rier)?|Ma(r(s)?|y)|Avr(il)?|(Mai)|Ju((in?)|)|Juillet|Ao(û|u)?t|Oct(obre)?|(Sep(?=\b|t)t?|Nov|D(é|e)?c)(embre)?)\ ((1[6-9]|[2-9]\d)\d{2})$/');
    // ^ Don't work for now because the delimiter is a space ^

    define('REGEX_EMAIL', '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i');
    define('REGEX_POSTALCODE', '/^[0-9]{4}0/'); // This should be use at the end of the sorting
    define("REGEX_PHONE", '/^(?:(?:\+|00)33|0)\s*[0-9](?:[\s.-]*\d{2}){4}$/');
    
    define("REGEX_PRICE", '/([0-9]+([,][0-9]{2})?([ ])?[€$£])|([€$£]([ ])?[0-9]+([,][0-9]{2})?)/'); // Only accept €, $ and £
    define("REGEX_PERCENT", '/^(100|[0-9]?[0-9])(([,.])([0-9]?[0-9]))?[ ]?%$/');

    define("REGEX_SIRET", '/^\d{3}[ ]?\d{3}[ ]?\d{3}[ ]?\d{5}$/');
    define("REGEX_WEBSITE", '/^(http(s)?:\/\/)?(www.)?([a-zA-Z0-9]*)(\.([a-zA-Z]))?(\.[a-z]{2,3})$/i'); // With or without http(s), include website.gouv.fr

    define("REGEX_INVOICE_NUMBER", '/^$/'); // Need the format use by the company to make the regex
    define("REGEX_FR_TVA_NUMBER", '/^(FR)([0-9]{11})$/');

?>