<?php

include('regex.php');

class SmartReader
{
    
    private $smart_content = array(
        'phones' => [],
        'emails' => [],
        'dates' => [],
        'postal_codes' => [],
        'prices' => [],
        'TVA_numbers' => [],
        'SIRET' => [],
        'websites' => [],
        'percentages' => [],
        'others' => []
    );

    private $time_start;
    private $time_end;

    public function __construct($filename)
    {
        $this->time_start = microtime(true);

        $file_content = file_get_contents($filename);
        $exploded = preg_split('/[\s]+/', $file_content); //explode(PHP_EOL, $file_content);
        
        foreach($exploded as $line)
        {
            $this->verify($line);
            // $line_explode = explode(' ', $line);
            // foreach($line_explode as $word)
            // {
            //     $this->verify($word);
            // }
        }

        $this->time_end = microtime(true);
    }

    public function verify($word)
    {
        if ($this->isPhone($word))
        {
            array_push($this->smart_content['phones'], rtrim($word));
        }
        else if ($this->isEmail($word))
        {
            array_push($this->smart_content['emails'], rtrim($word));
        }
        // Website verification is less restrictive than email's
        else if ($this->isWebsite($word))
        {
            array_push($this->smart_content['websites'], rtrim($word));
        }

        else if ($this->isDate($word))
        {
            array_push($this->smart_content['dates'], rtrim($word));
        }
        
        else if ($this->isPrice($word))
        {
            array_push($this->smart_content['prices'], rtrim($word));
        }

        else if ($this->isPercentage($word))
        {
            array_push($this->smart_content['percentages'], rtrim($word));
        }
        
        // TVA number is more restrictive than SIRET
        else if ($this->isTvaNumber($word))
        {
            array_push($this->smart_content['TVA_numbers'], rtrim($word));
        }
        // Siret is more restrictive than postal code
        else if ($this->isSiret($word))
        {
            array_push($this->smart_content['SIRET'], rtrim($word));
        }
        // Postal code verification is the least restrictive of all REGEXs
        else if ($this->isPostalCode($word))
        {
            array_push($this->smart_content['postal_codes'], rtrim($word));
        }
        else 
        {
            array_push($this->smart_content['others'], rtrim($word));
        }
    }

    private function isDate($subject)
    {
        return preg_match_all(REGEX_DATE, $subject) || preg_match(REGEX_DATE_LONG, $subject) || preg_match(REGEX_DATE_MONTH, $subject) || preg_match(REGEX_DATE_INVERT, $subject);
    }

    private function isEmail($subject)
    {
        return preg_match_all(REGEX_EMAIL, $subject);
    }

    private function isPostalCode($subject)
    {
        return preg_match_all(REGEX_POSTALCODE, $subject);
    }

    private function isPhone($subject)
    {
        return preg_match_all(REGEX_PHONE, $subject);
    }

    private function isPrice($subject)
    {
        return preg_match(REGEX_PRICE, $subject);
    }

    private function isSiret($subject)
    {
        return preg_match_all(REGEX_SIRET, $subject);
    }

    private function isWebsite($subject)
    {
        return preg_match_all(REGEX_WEBSITE, $subject);
    }

    private function isTvaNumber($subject)
    {
        return preg_match_all(REGEX_FR_TVA_NUMBER, $subject);
    }

    private function isPercentage($subject)
    {
        return preg_match_all(REGEX_PERCENT, $subject);
    }

    public function getSmartContent()
    {
        return $this->smart_content;
    }

    public function getExecTime() {
        return number_format( (($this->time_end - $this->time_start)*100), 4);
    }

    public function getProcessInfo()
    {
        return 'Temps d\'execution : ' . $this->getExecTime() . 'ms - Mémoire allouée : ' . number_format((memory_get_usage()*8/100000), 2) . 'Mo - Pique de mémoire : ' . number_format((memory_get_peak_usage()*8/100000), 2) .'Mo';
    }
}


?>