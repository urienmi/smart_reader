<?php

class Performance {

    private $time_start;
    private $time_end;

    public function start() {
        $this->time_start = hrtime(true);
    }

    public function stop() {
        $this->time_end = hrtime(true);
    }

    public function getExecutionTime() {
        return number_format((($this->time_end - $this->time_start)/1e+6), 4);
    }

    public function getResult() {
        if(!$this->time_start) {
            throw new Exception('start method is not defined');
        } else if(!$this->time_end) {
            throw new Exception('stop method is not defined');
        } else {
            echo "<div style='font-family: Arial; padding: 6px 8px; background-color: black; color: white;'>Execution time: <span style='font-family: Consolas; color: orange;'>" . $this->getExecutionTime() . "</span> ms</div>";
        }
    }

}
